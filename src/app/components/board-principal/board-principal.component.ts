import { Component, OnInit } from '@angular/core';
import { cards } from 'src/app/models/card.model';
import { CardsService } from 'src/app/services/cards.service';
import { LoginServiceService } from 'src/app/services/login-service.service';


@Component({
  selector: 'app-board-principal',
  templateUrl: './board-principal.component.html',
  styleUrls: ['./board-principal.component.css']
})
export class BoardPrincipalComponent implements OnInit {
  
  public cardsToDo: any;
  public cardsDoing: any;
  public cardsDone: any;
  public titulo: any;
  public conteudo: any;
  constructor(public loginService: LoginServiceService, public cardsService: CardsService) { }

  ngOnInit(): void {
    this.login();
  }

  login(){
    this.loginService.login("letscode","lets@123").subscribe(response=>{
      localStorage.setItem('id_token', response);
      this.buscarCards();
    })
  }

  adicionarCard(){
    this.cardsService.adicionarCard(this.titulo, this.conteudo, 'ToDo').subscribe(response=>{
      console.log(response);
      this.buscarCards();
    })
    
  }
  buscarCards(){
    this.cardsService.buscarCards().subscribe(response=>{ 
      this.cardsToDo = this.filtrarCards(response, 'ToDo');
      this.cardsDoing = this.filtrarCards(response, 'Doing');
      this.cardsDone = this.filtrarCards(response, 'Done');
    })
  }

  deletarCard(id: any){
    this.cardsService.deletarCard(id).subscribe(response=>{
      console.log(response);
      this.buscarCards();
    })
    
  }

  moverCard(posicao:any, card:any){
    this.cardsService.editarCard(card.id, card.titulo, card.conteudo, posicao).subscribe(response=>{
      console.log(response);
      this.buscarCards();
    });

  }

  editarCard(card:any){
    this.cardsService.editarCard(card.id, card.titulo, card.conteudo, card.lista).subscribe(response=>{
      console.log(response);
      this.buscarCards();
    });

  }
  filtrarCards(listaCards: any, tipo: string){
      let i = 0; 
      let cardsSelecionados: any = [];
      for (i =0; i < listaCards.length; i++){
        if(listaCards[i].lista == tipo){
          cardsSelecionados.push(listaCards[i])
        }
      }
      return cardsSelecionados;
  } 
}
