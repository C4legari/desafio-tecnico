import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardPrincipalComponent } from './board-principal.component';

describe('BoardPrincipalComponent', () => {
  let component: BoardPrincipalComponent;
  let fixture: ComponentFixture<BoardPrincipalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BoardPrincipalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BoardPrincipalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
