import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {
  ;

  constructor(private http: HttpClient) { } 
  login(usuario: string, senha: string): Observable<any> { 
    return this.http.post('http://192.168.0.49:4000/login', { login: usuario, senha: senha });
  }
}


