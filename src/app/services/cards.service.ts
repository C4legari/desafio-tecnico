import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor(private http: HttpClient) { }

  public buscarCards(){
    let token = this.getToken();
    return this.http.get('http://192.168.0.49:4000/cards/', {
      headers: new HttpHeaders().set('Authorization', `${token}`)
    } );
  }

  public adicionarCard(titulo: string, conteudo: string, lista: string){
    let token = this.getToken();
    return this.http.post('http://192.168.0.49:4000/cards/',  { 
      titulo : titulo,  
      conteudo: conteudo, 
        lista: lista,
      }
      , {headers: new HttpHeaders().set('Authorization', `${token}`)}
  )
  }
  public editarCard(id: any,titulo: string, conteudo: string, lista: string){
    let token = this.getToken();
    return this.http.put(`http://192.168.0.49:4000/cards/${id}`,  { 
      id: id,
      titulo : titulo,  
      conteudo: conteudo, 
        lista: lista,
      }
      , {headers: new HttpHeaders().set('Authorization', `${token}`)}
  )
  }
  public deletarCard(id: any){
    let token = this.getToken();
    return this.http.delete(`http://192.168.0.49:4000/cards/${id}`,{headers: new HttpHeaders().set('Authorization', `${token}`)}
  )
  }
  private getToken(){
    let retorno = localStorage.getItem('id_token')
    if(retorno == null){
      return ''
    }else{
      return retorno ;
    }
    
  }
}