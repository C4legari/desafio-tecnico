import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BoardPrincipalComponent } from './components/board-principal/board-principal.component';

const routes: Routes = [
  {
    path:"",
    component: BoardPrincipalComponent,
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
